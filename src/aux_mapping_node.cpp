#include <ros/ros.h>
#include <edt/aux_mapper.h>
int main(int argc, char **argv)
{
    ros::init(argc, argv, "aux_mapping_node");

    AuxMapper mapper;

    ros::spin();
    return 0;
}
