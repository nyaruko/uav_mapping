#include "edt/laser_updater.h"

LaserUpdater::LaserUpdater(const LaserParams &param):
  m_param(param)
{
  m_depth_data_sz = sizeof(LASER_DATA_TYPE) * static_cast<size_t>(m_param.scan_num);
  CUDA_ALLOC_DEV_MEM(&m_depth_data,m_depth_data_sz);
}


LaserUpdater::~LaserUpdater()
{
  CUDA_FREE_DEV_MEM(m_depth_data);
}


void LaserUpdater::update_occumap(GridMap *glb_map, const tf::Transform &trans, const sensor_msgs::LaserScan::ConstPtr &scan,
                                  bool assume_uniform_vertical_world, const int map_ctt)
{
  // construct the projection matrix
  tf::Quaternion Rotq=trans.getRotation();
  tf::Vector3 transVec=trans.getOrigin();

  ProjParams proj;
  proj.L2G = cudaMat::SE3<float>(static_cast<float>(Rotq.w()),static_cast<float>(Rotq.x()),
                                 static_cast<float>(Rotq.y()),static_cast<float>(Rotq.z()),
                                 static_cast<float>(transVec.m_floats[0]),
                                 static_cast<float>(transVec.m_floats[1]),
                                 static_cast<float>(transVec.m_floats[2]));
  proj.G2L = proj.L2G.inv();
  proj.center.x = static_cast<float>(trans.getOrigin().x());
  proj.center.y = static_cast<float>(trans.getOrigin().y());
  proj.center.z = static_cast<float>(trans.getOrigin().z());

  // copy detph data to device
  LASER_DATA_TYPE* depths = (LASER_DATA_TYPE*)&scan->ranges.at(0);
  CUDA_MEMCPY_H2D(m_depth_data,depths,m_depth_data_sz);

  GPU_SENS::update_occupancy_with_laser(glb_map,m_depth_data,proj,m_param,assume_uniform_vertical_world,map_ctt);
}
