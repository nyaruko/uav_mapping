#include "edt/aux_mapper.h"
#include "edt/gpu_edt.cuh"
AuxMapper::AuxMapper()
{
  // Load all the params
  m_nh.param<bool>("/edt/use_2D_map", m_2D_map_mode, false);
  m_nh.param<float>("/nndp_cpp/fly_height",m_fly_height,1.0f);
  m_nh.param<float>("/edt/2D_compress_range",m_compress_range,0.2f);
  m_nh.param<bool>("/edt/assume_uniform_vertical",m_assume_uniform_vertical_world,false);
  m_nh.param<bool>("/edt/nan_is_infinity",m_nan_is_infinity,false);

  // Publisher
  m_edt_pub = m_nh.advertise<cpc_aux_mapping::grid_map> ("edt_map", 1);

  // Subscriber
  m_confi_img_sub = m_nh.subscribe("zed/confidence/confidence_map", 1, &AuxMapper::get_confidnece_map, this);
  m_caminfo_sub = m_nh.subscribe("/revised_sensor/camera_info",1, &AuxMapper::get_caminfo, this);

#ifdef USE_SYNC
  m_laser_sub.subscribe(m_nh,"/scan", 1);
  m_odom_sub.subscribe(m_nh,"/odom", 1);
  m_sync = new message_filters::Synchronizer<MySyncPolicy> (MySyncPolicy(130), m_laser_sub, m_odom_sub);
  m_sync->setMaxIntervalDuration(ros::Duration(0.025));
  m_sync->registerCallback(boost::bind(&AuxMapper::get_laser_odom,this,_1,_2));
#else
  m_laser_sub = m_nh.subscribe("/scan", 1, &AuxMapper::get_laser_scan, this);
  m_odom_sub = m_nh.subscribe("/odom",1,&AuxMapper::get_vehicle_odom,this);
  m_depth_img_sub = m_nh.subscribe("/revised_sensor/image",1,&AuxMapper::get_depth_img, this);
#endif
  // Timer
  m_map_timer = m_nh.createTimer(ros::Duration(0.1), &AuxMapper::construct_map, this);

  // Normal initialization of objects and flags
  m_laser_scan = boost::shared_ptr<sensor_msgs::LaserScan>(new sensor_msgs::LaserScan());
  m_pose = boost::shared_ptr<geometry_msgs::PoseStamped>(new geometry_msgs::PoseStamped());
  m_depth_img=boost::shared_ptr<sensor_msgs::Image>(new sensor_msgs::Image());
  m_confi_img=boost::shared_ptr<sensor_msgs::Image>(new sensor_msgs::Image());//0--100
  m_got_scan = false;
  m_got_pose = false;
  m_got_depth=false;
  m_got_confi_map=false;
  m_got_caminfo=false;
  m_map_ctt = 0;

  // Setup the global map
  // Map parameters
  float grid_size = 0.2f;
  CUDA_GEO::pos origin(0,0,0);
  int3 glb_map_size = make_int3(300,300,30);
  int3 loc_map_size = make_int3(100,100,30);
  // Map object
  m_glb_map = new GridMap(origin,grid_size,glb_map_size,loc_map_size);
  m_glb_map->setup_device();

  if (m_2D_map_mode)
  {
    // Add a 2D occupancy map, it is constructed by compressing the glb_map within
    // certain height to a single layer
    int3 glb_map_2d_size = make_int3(glb_map_size.x, glb_map_size.y, 1);
    int3 loc_map_2d_size = make_int3(loc_map_size.x, loc_map_size.y, 1);
    m_2D_glb_map = new GridMap(origin, grid_size, glb_map_2d_size, loc_map_2d_size);
    m_2D_glb_map->setup_device();

    // If we use 2D map mode the local map has a height of ONE only.
    loc_map_size.z = 1;
  }

  // Setup the EDT map
  m_edt_map = new EDTMap(origin,grid_size,loc_map_size);
  m_edt_map->setup_device();

  // Setup the cutt rotations
  if (m_2D_map_mode)
  {
    // 2D rotation mode
    int m = m_edt_map->m_map_size.x;
    int n = m_edt_map->m_map_size.y;
    int dim_0[2] = {m,n};
    int permu_0[2] = {1,0};
    int dim_1[2] = {n,m};
    int permu_1[2] = {1,0};
    cuttPlan(&m_rot_plan[0], 2, dim_0, permu_0, sizeof(int), nullptr);
    cuttPlan(&m_rot_plan[1], 2, dim_1, permu_1, sizeof(int), nullptr);
  }
  else
  {
    // 3D rotation mode
    int m = m_edt_map->m_map_size.x;
    int n = m_edt_map->m_map_size.y;
    int p = m_edt_map->m_map_size.z;
    int dim_0[3] = {m,n,p};
    int permu_0[3] = {1,0,2};
    int dim_1[3] = {n,m,p};
    int permu_1[3] = {0,2,1};
    int dim_2[3] = {n,p,m};
    int permu_2[3] = {2,0,1};
    cuttPlan(&m_rot_plan[0], 3, dim_0, permu_0, sizeof(int), nullptr);
    cuttPlan(&m_rot_plan[1], 3, dim_1, permu_1, sizeof(int), nullptr);
    cuttPlan(&m_rot_plan[2], 3, dim_2, permu_2, sizeof(int), nullptr);
  }

  // Setup the map message
  setup_map_msg(m_edt_map_msg, m_edt_map, true);

#ifdef SHOWPC
  m_view_pnt_cld = PntCld::Ptr(new PntCld);
  m_view_pnt_cld->header.frame_id = "/world";
  m_pnt_cld_pub = m_nh.advertise<PntCld> ("edt_map_clound", 1);
  m_view_sd_map = new SeenDist[loc_map_size.x*loc_map_size.y*loc_map_size.z];
#endif

  // Start the mapping timer
  m_map_timer.start();
}

AuxMapper::~AuxMapper()
{
  m_glb_map->free_device();
  delete m_glb_map;

  m_edt_map->free_device();
  delete m_edt_map;

  if (m_2D_map_mode)
  {
    m_2D_glb_map->free_device();
    delete m_2D_glb_map;
  }

  if (m_camera_updater != nullptr)
    delete m_camera_updater;

  if (m_laser_updater != nullptr)
    delete m_laser_updater;

  for (int i=0;i<3;i++)
    cuttDestroy(m_rot_plan[i]);

#ifdef SHOWPC
  delete [] m_view_sd_map;
#endif

#ifdef USE_SYNC
  delete m_sync;
#endif
}

#ifdef USE_SYNC
void AuxMapper::get_laser_odom(const sensor_msgs::LaserScan::ConstPtr &lsr, const nav_msgs::Odometry::ConstPtr &odom)
{
  get_laser_scan(lsr);
  get_vehicle_odom(odom);
}
#endif

void AuxMapper::get_caminfo(const sensor_msgs::CameraInfo::ConstPtr& msg)
{
  if (!m_got_caminfo)
  {
    float cx = static_cast<float>(msg->K[2]);
    float cy = static_cast<float>(msg->K[5]);
    float fx = static_cast<float>(msg->K[0]);
    float fy = static_cast<float>(msg->K[4]);
    int depth_rows = static_cast<int>(msg->height);
    int depth_cols = static_cast<int>(msg->width);
    ROS_INFO_STREAM("Received calibration: " << "cx=" << cx << " cy= " << cy << " fx=" << fx << " fy=" << fy << " width=" << depth_cols << " height=" << depth_rows);
    CameraParams param(depth_rows,depth_cols,cx,cy,fx,fy,m_nan_is_infinity);
    m_camera_updater=new CameraUpdater(param);
  }
  m_got_caminfo = true;
}

void AuxMapper::construct_map(const ros::TimerEvent&)
{
  // Have to the pose, and at least one sensor measurement
  if (!m_got_pose || (!m_got_scan && !m_got_depth))
    return;

  auto start = std::chrono::steady_clock::now();

  m_map_ctt++;
  // Construct a transfrom from body to world using current the pose
  tf::Quaternion quat;
  tf::quaternionMsgToTF(m_pose->pose.orientation, quat);
  tf::Transform trans;
  trans.setRotation(quat);
  trans.setOrigin(tf::Vector3(m_pose->pose.position.x,
                              m_pose->pose.position.y,
                              m_pose->pose.position.z));

  // Update occupancy map using the 2D laser
  if (m_got_scan)
  {
    m_laser_updater->update_occumap(m_glb_map,trans,m_laser_scan,m_assume_uniform_vertical_world,m_map_ctt);
  }

  // Update the occupancy using depth image
  if (m_got_depth)
  {
    m_camera_updater->update_occumap(m_glb_map,trans,m_depth_img,m_map_ctt);
  }

  // Calculate distance transform information
  CUDA_GEO::pos center;
  if (m_2D_map_mode)
    center = calculate_edt_2d();
  else
    center = calculate_edt_3d();

  // Copy the EDT to host memory and publish the seen distance map
  CUDA_MEMCPY_D2H(m_edt_map_msg.payload8.data(),m_edt_map->m_sd_map,static_cast<size_t>(m_edt_map->m_byte_size));
  setup_map_msg(m_edt_map_msg, m_edt_map, false);
  m_edt_pub.publish (m_edt_map_msg);

  auto end = std::chrono::steady_clock::now();
  std::cout << "Dense mapping time: "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
            << " ms" << std::endl;

#ifdef SHOWPC
  plot_pnt_cld(center);
#endif
}

CUDA_GEO::pos AuxMapper::calculate_edt_2d()
{
  // Assign the local map center (as the current vehicle horizon position + desired fly height)
  CUDA_GEO::pos center(m_pose->pose.position.x, m_pose->pose.position.y, m_fly_height);

  // Calculate the pivot point (the global coordinate of the origin of the local map)
  CUDA_GEO::coord pvt = calculate_pivot(center);

  // Set the edt map's origin
  m_edt_map->m_origin = m_glb_map->coord2pos(pvt);

  // Note that we use the same pvt as the shift for the following two kernels
  // so that during EDT it could read the correct data.
  // Compress the 3D map into 2D
  GPU_EDT::compress_3D_map_to_2D(m_glb_map,m_2D_glb_map,pvt,m_compress_range,m_map_ctt);

  // Use the compressed 2D map to calculate the EDT
  GPU_EDT::edt_2D(m_2D_glb_map,m_edt_map,pvt,m_rot_plan,m_map_ctt);

  return center;
}

CUDA_GEO::pos AuxMapper::calculate_edt_3d()
{
  // Assign the local map center (as the current vehicle position)
  CUDA_GEO::pos center(m_pose->pose.position.x, m_pose->pose.position.y, m_pose->pose.position.z);

  // Calculate the pivot point (the global coordinate of the origin of the local map)
  CUDA_GEO::coord pvt = calculate_pivot(center);

  // Set the edt map's origin
  m_edt_map->m_origin = m_glb_map->coord2pos(pvt);

  // Perfrom EDT (Euclidean distance transform)
  GPU_EDT::edt_3D(m_glb_map,m_edt_map,pvt,m_rot_plan,m_map_ctt);

  return center;
}

void AuxMapper::get_laser_scan(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  // create the laser updater from the scan message
  if (!m_got_scan)
  {
    LaserParams param(static_cast<int>(msg->ranges.size()),10.0,msg->angle_increment,msg->angle_min);
    m_laser_updater = new LaserUpdater(param);
  }
  m_got_scan = true;
  *m_laser_scan = *msg;
}

void AuxMapper::get_vehicle_pose(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
  m_got_pose = true;
  *m_pose = *msg;
}

void AuxMapper::get_vehicle_odom(const nav_msgs::Odometry::ConstPtr &msg)
{
  m_got_pose = true;
  m_pose->pose = msg->pose.pose;
}

void AuxMapper::get_confidnece_map(const sensor_msgs::Image::ConstPtr& msg)
{

}

void AuxMapper::get_depth_img(const sensor_msgs::Image::ConstPtr& msg)
{
  // Only start to read in the depth image after we have recevied the
  // camera info (m_got_caminfo) in function "get_caminfo"
  if (m_got_caminfo)
  {
    m_got_depth=true;
    *m_depth_img=*msg;
  }
}

// Pivot point: the global-map coordinate of the local map's origin.
CUDA_GEO::coord AuxMapper::calculate_pivot(const CUDA_GEO::pos &vehicle_pos)
{
  CUDA_GEO::coord c = m_glb_map->pos2coord(vehicle_pos);
  c.x -= m_edt_map->m_map_size.x/2;
  c.y -= m_edt_map->m_map_size.y/2;
  c.z -= m_edt_map->m_map_size.z/2;
  return c;
}

void AuxMapper::setup_map_msg(cpc_aux_mapping::grid_map &msg, EDTMap* map, bool resize)
{
  // Setup size the grid width
  msg.x_origin = map->m_origin.x;
  msg.y_origin = map->m_origin.y;
  msg.z_origin = map->m_origin.z;
  msg.width = map->m_grid_step;

  if (resize)
  {
    msg.x_size = map->m_map_size.x;
    msg.y_size = map->m_map_size.y;
    msg.z_size = map->m_map_size.z;
    msg.payload8.resize(sizeof(SeenDist)*static_cast<unsigned int>(msg.x_size*msg.y_size*msg.z_size));
  }

  msg.type = cpc_aux_mapping::grid_map::TYPE_EDT;
}
