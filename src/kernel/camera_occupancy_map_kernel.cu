#include "edt/gpu_sensors.cuh"
#include "shared_kernel.hpp"

#define LARGE_FLOAT 1000.f
namespace GPU_SENS
{
__device__ __forceinline__
void global2local(const CUDA_GEO::pos &vox_pos,const ProjParams &proj,
                    const CameraParams &param, int2 &pix, float &depth)
{
  float3 vox_pos_f3=make_float3(vox_pos.x,vox_pos.y,vox_pos.z);

  // from global frame to sensor frame
  float3 camPoint=proj.G2L*vox_pos_f3;     // 3d pose in camera axis

  // get the depth
  depth=camPoint.x;

  // calculate the pixel coordinate on the image
  pix.x=floor(-camPoint.y*param.fx/depth+param.cx+0.5);
  pix.y=floor(-camPoint.z*param.fy/depth+param.cy+0.5);
}

__device__ __forceinline__
void local2global(const ProjParams &proj, const CameraParams &param,
                  const int2 &pix, const float &depth,
                  CUDA_GEO::pos &glb_pos)
{
  //From pix to camera frame
  float3 camPoint;
  camPoint.x = depth;
  camPoint.y = (param.cx - pix.x)*depth/param.fx;
  camPoint.z = (param.cy - pix.y)*depth/param.fy;

  camPoint = proj.L2G*camPoint;
  glb_pos.x = camPoint.x;
  glb_pos.y = camPoint.y;
  glb_pos.z = camPoint.z;
}
//===
__global__
void update_status(GridMap glb_map,
                      CAM_DATA_TYPE *d_depth,
                      ProjParams proj,
                      CameraParams param,
                      CUDA_GEO::coord shift,
                      int map_ctt)
{
  CUDA_GEO::coord local_c, glb_c;
  float proj_depth, mes_depth;
  CUDA_GEO::pos vox_pos;
  int2 pix;

  local_c.z = blockIdx.x;
  local_c.y = threadIdx.x;

  for (local_c.x = 0; local_c.x < glb_map.m_update_range.x; ++local_c.x)
  {
    // get global cooridnate
    glb_c = local_c + shift;

    // check whether this cooridnate has been registed by point cloud
    if (glb_map.registered_times(glb_c) > 0)
    {
      glb_map.unregister_pcl(glb_c);
      glb_map.set_status(glb_c, GridMap::OCCUPIED);
      continue;
    }

    // get the the position of the coord center
    vox_pos=glb_map.coord2pos(glb_c);

    // get the nearest pixel on the depth image, and the "depth" of point vox_pos
    global2local(vox_pos, proj, param, pix, proj_depth);

    if (proj_depth <= 0.3 || proj_depth >9.0 ||
        pix.x < 0 || pix.x >= param.cols || pix.y < 0 || pix.y >= param.rows)
    {
      glb_map.set_status(glb_c, GridMap::OUT_OF_FRUSTUM);
      continue;
    }

    // get the true measured depth on the image
    mes_depth=d_depth[param.cols*pix.y+pix.x];

    if(param.nan_is_infinity && isnan(mes_depth))
      mes_depth = LARGE_FLOAT;

    // invalid measuement
    if (isnan(mes_depth))
    {
      glb_map.set_status(glb_c, GridMap::BLOCKED);
      continue;
    }

    if (proj_depth < mes_depth)
    {
      //in free space
      glb_map.set_status(glb_c, GridMap::FREE);
    }
    else
    {
      //blocked by obstacle
      glb_map.set_status(glb_c, GridMap::BLOCKED);
    }
  }
}
//===
__global__
void update_occupancy_direct(GridMap glb_map,
                             CAM_DATA_TYPE *d_depth,
                             ProjParams proj,
                             CameraParams param,
                             CUDA_GEO::coord shift,
                             int map_ctt)
{
  CUDA_GEO::coord local_c, glb_c;
  float proj_depth, mes_depth;
  CUDA_GEO::pos vox_pos;
  int2 pix;

  local_c.z = blockIdx.x;
  local_c.y = threadIdx.x;

  for (local_c.x = 0; local_c.x < glb_map.m_update_range.x; ++local_c.x)
  {
    // get global cooridnate
    glb_c = local_c + shift;
    if (glb_map.registered_times(glb_c) > 0)
    {
      glb_map.unregister_pcl(glb_c);
      glb_map.update_occupancy(glb_c, 250, 0.8, map_ctt);
      continue;
    }

    // get the the position of the coord center
    vox_pos=glb_map.coord2pos(glb_c);

    // get the nearest pixel on the depth image, and the "depth" of point vox_pos
    global2local(vox_pos, proj, param, pix, proj_depth);

    // not in camera frustrum
    if (proj_depth <= 0.3 || proj_depth >9.0 ||
        pix.x < 0 || pix.x >= param.cols || pix.y < 0 || pix.y >= param.rows)
      continue;

    // get true measurement from the pix
    mes_depth=d_depth[param.cols*pix.y+pix.x];

    if(param.nan_is_infinity && isnan(mes_depth))
      mes_depth = LARGE_FLOAT;

    if (isnan(mes_depth) || mes_depth <= 0.21)
      continue;

    // free the grid
    if (proj_depth < mes_depth - 0.21)
    {
      glb_map.update_occupancy(glb_c, 0, 0.5, map_ctt);
    }
  }
}
//===
__global__
void register_point_cloud(GridMap glb_map,
                          CAM_DATA_TYPE *d_depth,
                          ProjParams proj,
                          CameraParams param,
                          CUDA_GEO::coord shift,
                          int map_ctt)
{
  int2 pix;
  pix.x = threadIdx.x;
  pix.y = blockIdx.x;
  float depth = d_depth[param.cols*pix.y+pix.x];
  CUDA_GEO::pos glb_pos;
  local2global(proj,param,pix,depth,glb_pos);
  CUDA_GEO::coord glb_c;
  glb_c = glb_map.pos2coord(glb_pos);

  // check whether this glb_c is inside the updat range volume
  CUDA_GEO::coord s = glb_c-shift;
  if (s.x<0 || s.x>=glb_map.m_update_range.x ||
      s.y<0 || s.y>=glb_map.m_update_range.y ||
      s.z<0 || s.z>=glb_map.m_update_range.z)
  {
    return;
  }

  //register the point cloud
  glb_map.register_pcl(glb_c);
}
//===
void update_occupancy_with_camera(GridMap *glb_map, CAM_DATA_TYPE *detph_data, ProjParams proj, CameraParams param, int map_ctt)
{
  const int gridSize = glb_map->m_update_range.z;
  const int blkSize = glb_map->m_update_range.y;

  // calculate the shift
  CUDA_GEO::coord shift = glb_map->pos2coord(proj.center);
  shift.x -= glb_map->m_update_range.x/2;
  shift.y -= glb_map->m_update_range.y/2;
  shift.z -= glb_map->m_update_range.z/2;

  register_point_cloud<<<param.rows,param.cols>>>(*glb_map,detph_data,proj,param,shift,map_ctt);
#ifdef USE_DIRECT_METHOD
  update_occupancy_direct<<<gridSize,blkSize>>>(*glb_map,detph_data,proj,param,shift,map_ctt);
#else

  update_status<<<gridSize,blkSize>>>(*glb_map,detph_data,proj,param,shift,map_ctt);
  const dim3 update_blk(16,16,4);
  const dim3 update_grid(ceil(1.0f*glb_map->m_update_range.x/update_blk.x),
                          ceil(1.0f*glb_map->m_update_range.y/update_blk.y),
                          ceil(1.0f*glb_map->m_update_range.z/update_blk.z));

  int filter_half_width = 1;

  int shared_size = (update_blk.x + 2*filter_half_width)*(update_blk.y + 2*filter_half_width)*(update_blk.z + 2*filter_half_width) * sizeof(unsigned char);
  update_occupancy_from_status<<<update_grid,update_blk,shared_size>>>(*glb_map,shift,filter_half_width,map_ctt);
#endif

}
}

