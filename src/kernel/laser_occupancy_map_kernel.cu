#include "edt/gpu_sensors.cuh"
#include "shared_kernel.hpp"

namespace GPU_SENS
{
struct LaserFramePoint
{
  float3 p;
  float theta;
  float r;
};

__device__ __forceinline__
int positive_modulo(int i, int n)
{
  return (i % n + n) % n;
}
//---
__device__ __forceinline__
float in_pi(float in)
{
  return in - floorf((in + CUDA_F_PI) / (2 * CUDA_F_PI)) * 2 * CUDA_F_PI;
}
//===
__device__ __forceinline__
void global2local(const CUDA_GEO::pos &vox_pos,
                  const ProjParams &proj,
                  LaserFramePoint &pnt)
{
  float3 vox_pos_f3 = make_float3(vox_pos.x,vox_pos.y,vox_pos.z);
  pnt.p = proj.G2L*vox_pos_f3;
  pnt.theta = atan2(pnt.p.y, pnt.p.x);
  pnt.r = sqrtf(dot(pnt.p,pnt.p));
  //pnt.flat_r = sqrtf(pnt.p.x*pnt.p.x + pnt.p.y*pnt.p.y);
}

__device__ __forceinline__
void local2global(const ProjParams &proj,
    const float &theta,
    const float &r,
    CUDA_GEO::pos &glb_pos)
{
  float3 pnt = make_float3(cosf(theta)*r, sinf(theta)*r, 0);
  pnt = proj.L2G*pnt;
  glb_pos.x = pnt.x;
  glb_pos.y = pnt.y;
  glb_pos.z = pnt.z;
}

__device__ __forceinline__
float3 dist2ray(const LaserFramePoint &pnt, const int &ray_id,  const LaserParams &param)
{
  // get the theta of the ray
  float ray_theta = param.theta_min + ray_id*param.theta_inc;

  // get the theta difference between the ray and the point
  float theta_diff = in_pi(ray_theta - pnt.theta);

  // get the distance from the point to they ray in the laser x-y frame
  float flat_dist = sinf(theta_diff) * sqrtf(pnt.p.x*pnt.p.x + pnt.p.y*pnt.p.y);

  // return a float3
  // x: true distance
  // y: horizontal (flat) distance
  // z: vertical distance
  return make_float3(sqrtf(flat_dist*flat_dist + pnt.p.z*pnt.p.z),
                     fabsf(flat_dist),
                     fabsf(pnt.p.z));
}
//===
__device__ __forceinline__
float interpol_mes_depth(float theta, const LaserParams &param, LASER_DATA_TYPE *detph_data)
{
  float mes_depth;

  //get the left neighbour ray id
  theta = in_pi(theta);
  int left_idx = floorf((theta - param.theta_min)/param.theta_inc);
  left_idx = positive_modulo(left_idx, param.scan_num);

  //get the right neighbour ray id
  int right_idx = left_idx + 1;
  right_idx = positive_modulo(right_idx, param.scan_num);

  // get the left ray depth
  float left_depth = detph_data[left_idx];
  if (left_depth > 1e5f)
    left_depth = 1e5f;

  // get the right ray depth
  float right_depth = detph_data[right_idx];
  if (right_depth > 1e5f)
    right_depth = 1e5f;

  // estimate the measured depth
  if (isnan(left_depth) && isnan(right_depth))
  {
    mes_depth = NAN;
  }
  else if (!isnan(left_depth) && isnan(right_depth))
  {
    mes_depth = left_depth;
  }
  else if (isnan(left_depth) && !isnan(right_depth))
  {
    mes_depth = right_depth;
  }
  else
  {
    if (fabsf(right_depth - left_depth) < 0.15f)
    {
      // interpolation
      float w_left = fabsf(in_pi(right_idx*param.theta_inc+param.theta_min - theta)/param.theta_inc);
      float w_right = fabsf(in_pi(theta - left_idx*param.theta_inc+param.theta_min)/param.theta_inc);
      mes_depth = w_left * left_depth + w_right*right_depth;
    }
    else
    {
      // know nothing, return NAN
      mes_depth = min(left_depth, right_depth);
    }
  }
  return mes_depth;
}
//===
__device__ __forceinline__
float calc_horizon_dist(const CUDA_GEO::pos &pa, const CUDA_GEO::pos &pb)
{
  return sqrtf((pa.x - pb.x)*(pa.x - pb.x) + (pa.y - pb.y)*(pa.y - pb.y));
}
//===
__global__
void update_occupancy_direct(GridMap glb_map, LASER_DATA_TYPE *detph_data, ProjParams proj,
                      LaserParams param, CUDA_GEO::coord shift, bool assume_uniform_vertical_world, int map_ctt)
{
  CUDA_GEO::coord local_c;
  local_c.z = blockIdx.x;
  local_c.y = threadIdx.x;

  CUDA_GEO::coord glb_c;
  CUDA_GEO::pos vox_pos;
  LaserFramePoint laser_frame_pnt;
  int closest_ray_id;
  float3 dist_to_ray; // x: true dist, y: horizon dist, z: vertical dist
  float mes_depth;
  for (local_c.x = 0; local_c.x < glb_map.m_update_range.x; ++local_c.x)
  {
    // get global coordinate
    glb_c = local_c + shift;

    if (glb_map.registered_times(glb_c) > 0)
    {
      glb_map.unregister_pcl(glb_c);
      glb_map.update_occupancy(glb_c, 250, 0.8, map_ctt);
      continue;
    }

    //get global position of the voxel
    vox_pos=glb_map.coord2pos(glb_c);

    //get point position in the laser frame
    global2local(vox_pos, proj, laser_frame_pnt);

    //get the closest_ray_id ray in the laser
    closest_ray_id = floor((laser_frame_pnt.theta - param.theta_min)/param.theta_inc +0.5f);

    //get distance to the closest_ray
    //x: true dist, y: horizon dist, z: vertical dist
    dist_to_ray = dist2ray(laser_frame_pnt, closest_ray_id, param);

    if (assume_uniform_vertical_world)
    {
      // if assume a uniform vertical world
      // we pretend the laser ray is a vertical band with bandwidth = 0.6, thickness = 0.2
      // and centered at the original ray
      if (dist_to_ray.y > 0.1f || dist_to_ray.z > 0.3f)
        continue;
    }
    else
    {
      if (dist_to_ray.x > 0.1f)
        continue;
    }

    mes_depth = interpol_mes_depth(laser_frame_pnt.theta, param, detph_data);

    if (isnan(mes_depth) || mes_depth <= 0.3f)
      continue;

    if (assume_uniform_vertical_world)
    {
      // First project the "measured point" (in fact got through interpolation)
      // into the global frame (as mes_pos)
      CUDA_GEO::pos mes_pos;
      local2global(proj,laser_frame_pnt.theta,mes_depth,mes_pos);

      // Here we compare the horizontal distance from vox_pos and mes_pos to the vehicle center
      // Because we assume the vertical dimension is uniform
      //Vehicle center                                                     Wall
      //|                                                                    |
      //|                                                                    |
      //|-\                                                                  |
      //|  ---\                                                              |
      //|      ---\                                                          |
      //|          ----\                                                     |
      //|               ---\                                                 |
      //|                   ---\                                             |
      //|                       ----\  laser ray                             |
      //|                            ---\                                    |
      //|                                ---\                                |
      //|                                    ---\                            |
      //|                                        ----\               vox_pos |
      //|----------------------------------------------------------------*   |
      //|                           horizon_dist_1        ---\               |
      //|                                                     ----\          |
      //|                                                          ---\      |
      //|                                                              ---\  |
      //|--------------------------------------------------------------------* mes_pos
      //|                           horizon_dist_2                           |
      // horizon_dist_1 < horizon_dist_2 : shall be cleared

      if (calc_horizon_dist(vox_pos,proj.center) < calc_horizon_dist(mes_pos,proj.center) - 0.2f)
        glb_map.update_occupancy(glb_c, 0, 0.5, map_ctt);
    }
    else
    {
      if (laser_frame_pnt.r < mes_depth - 0.2f)
          glb_map.update_occupancy(glb_c, 0, 0.5, map_ctt);
    }

  }
}
//===
__global__
void update_status(GridMap glb_map, LASER_DATA_TYPE *detph_data, ProjParams proj,
                      LaserParams param, CUDA_GEO::coord shift, bool assume_uniform_vertical_world, int map_ctt)
{
  CUDA_GEO::coord local_c;
  local_c.z = blockIdx.x;
  local_c.y = threadIdx.x;

  CUDA_GEO::coord glb_c;
  CUDA_GEO::pos vox_pos;
  LaserFramePoint laser_frame_pnt;
  int closest_ray_id;
  float3 dist_to_ray;
  float mes_depth;
  for (local_c.x = 0; local_c.x < glb_map.m_update_range.x; ++local_c.x)
  {
    // get global coordinate
    glb_c = local_c + shift;

    if (glb_map.registered_times(glb_c) > 0)
    {
      glb_map.unregister_pcl(glb_c);
      glb_map.set_status(glb_c, GridMap::OCCUPIED);
      continue;
    }

    //get global position of the voxel
    vox_pos=glb_map.coord2pos(glb_c);

    //get point position in the laser frame
    global2local(vox_pos, proj, laser_frame_pnt);

    //get the closest_ray_id ray in the laser
    closest_ray_id = floor((laser_frame_pnt.theta - param.theta_min)/param.theta_inc +0.5f);

    //get distance to the closest_ray
    dist_to_ray = dist2ray(laser_frame_pnt, closest_ray_id, param);


    if (assume_uniform_vertical_world)
    {
      // if assume a uniform vertical world
      // we pretend the laser ray is a vertical band with bandwidth = 0.6, thickness = 0.2
      // and centered at the original ray
      if (dist_to_ray.y > 0.1f || dist_to_ray.z > 0.3f)
      {
        glb_map.set_status(glb_c,GridMap::OUT_OF_FRUSTUM);
        continue;
      }
    }
    else
    {
      if (dist_to_ray.x > 0.1f)
      {
        glb_map.set_status(glb_c,GridMap::OUT_OF_FRUSTUM);
        continue;
      }
    }

    //get the actual measurment
    mes_depth = interpol_mes_depth(laser_frame_pnt.theta, param, detph_data);

    if (isnan(mes_depth))
    {
      glb_map.set_status(glb_c, GridMap::BLOCKED);
      continue;
    }

    if (assume_uniform_vertical_world)
    {
      // First project the "measured point" (in fact got through interpolation)
      // into the global frame (as mes_pos)
      CUDA_GEO::pos mes_pos;
      local2global(proj,laser_frame_pnt.theta,mes_depth,mes_pos);

      // Here we compare the horizontal distance from vox_pos and mes_pos to the vehicle center
      // Because we assume the vertical dimension is uniform
      // Check the graphical explanation in function "update_occupancy_direct"
      if (calc_horizon_dist(vox_pos,proj.center) < calc_horizon_dist(mes_pos,proj.center))
        glb_map.set_status(glb_c,GridMap::FREE);
      else
        glb_map.set_status(glb_c,GridMap::BLOCKED);
    }
    else
    {
      if (laser_frame_pnt.r < mes_depth)
        glb_map.set_status(glb_c,GridMap::FREE);
      else
        glb_map.set_status(glb_c,GridMap::BLOCKED);
    }
  }
}
//===
__global__
void register_point_cloud(GridMap glb_map, LASER_DATA_TYPE *detph_data, ProjParams proj,
                                LaserParams param, CUDA_GEO::coord shift, int map_ctt)
{
  int idx = threadIdx.x + blockIdx.x *blockDim.x ;
  if (idx >= param.scan_num)
    return;

  float theta =  param.theta_min + idx*param.theta_inc;
  float r= detph_data[idx];

  CUDA_GEO::pos glb_pos;
  local2global(proj,theta,r,glb_pos);
  CUDA_GEO::coord glb_c;
  glb_c = glb_map.pos2coord(glb_pos);

  // check whether this glb_c is inside the updat range volume
  CUDA_GEO::coord s = glb_c-shift;
  if (s.x<0 || s.x>=glb_map.m_update_range.x ||
      s.y<0 || s.y>=glb_map.m_update_range.y ||
      s.z<0 || s.z>=glb_map.m_update_range.z)
  {
    return;
  }

  //register the point cloud
  glb_map.register_pcl(glb_c);
}
//===
void update_occupancy_with_laser(GridMap *glb_map, LASER_DATA_TYPE *detph_data, ProjParams proj, LaserParams param, bool assume_uniform_vertical_world, int map_ctt)
{
  const int gridSize = glb_map->m_update_range.z;
  const int blkSize = glb_map->m_update_range.y;

  // calculate the shift
  CUDA_GEO::coord shift = glb_map->pos2coord(proj.center);
  shift.x -= glb_map->m_update_range.x/2;
  shift.y -= glb_map->m_update_range.y/2;
  shift.z -= glb_map->m_update_range.z/2;
\
  register_point_cloud<<<4,param.scan_num/4>>>(*glb_map, detph_data, proj, param, shift, map_ctt);
#ifdef USE_DIRECT_METHOD
  update_occupancy_direct<<<gridSize,blkSize>>>(*glb_map, detph_data, proj, param, shift, assume_uniform_vertical_world, map_ctt);
#else
  update_status<<<gridSize,blkSize>>>(*glb_map,detph_data,proj,param,shift,assume_uniform_vertical_world,map_ctt);
  const dim3 update_blk(16,16,4);
  const dim3 update_grid(ceil(1.0f*glb_map->m_update_range.x/update_blk.x),
                          ceil(1.0f*glb_map->m_update_range.y/update_blk.y),
                          ceil(1.0f*glb_map->m_update_range.z/update_blk.z));

  int filter_half_width = 1;

  int shared_size = (update_blk.x + 2*filter_half_width)*(update_blk.y + 2*filter_half_width)*(update_blk.z + 2*filter_half_width) * sizeof(unsigned char);
  update_occupancy_from_status<<<update_grid,update_blk,shared_size>>>(*glb_map,shift,filter_half_width,map_ctt);
#endif
}
}

