#include <edt/gpu_edt.cuh>

#define cuttCheck(stmt) do {                                 \
  cuttResult err = stmt;                            \
  if (err != CUTT_SUCCESS) {                          \
  fprintf(stderr, "%s in file %s, function %s\n", #stmt,__FILE__,__FUNCTION__); \
  exit(1); \
  }                                                  \
  } while(0)

namespace GPU_EDT
{
//phase 1: each kernel scan along the true y direction
__global__
void phase1(CUDA_GEO::coord shift, GridMap glb_map, EDTMap local_map, int map_ct)
{
  CUDA_GEO::coord c;
  c.z = blockIdx.x;
  c.x = threadIdx.x;

  unsigned char val = 0;
  bool seen = 0;

  // scan 1
  glb_map.get_pbty(&val,&seen,c+shift,map_ct);
  if (val > 160)
    local_map.g(c.x,0,c.z,0) = 0;
  else
    local_map.g(c.x,0,c.z,0) = local_map.m_max_width;

  for (c.y=1; c.y<local_map.m_map_size.y; c.y++)
  {
    glb_map.get_pbty(&val,&seen,c+shift,map_ct);
    if (val > 160)
      local_map.g(c.x,c.y,c.z,0) = 0;
    else
      local_map.g(c.x,c.y,c.z,0) = 1 + local_map.g(c.x,c.y-1,c.z,0);
  }
  // scan 2
  for (c.y=local_map.m_map_size.y-2;c.y>=0;c.y--)
  {
    if (local_map.g(c.x,c.y+1,c.z,0) < local_map.g(c.x,c.y,c.z,0))
    {
      local_map.g(c.x,c.y,c.z,0) = 1+local_map.g(c.x,c.y+1,c.z,0);
    }
  }
}

// phase 1: each kernel scan along the true y direction
// A special mode that only utilize the registered .o flag
// from the local map itself to determin whether a grid is "occupied"
__global__
void phase1_occupancy_mode(EDTMap local_map)
{
  CUDA_GEO::coord c;
  c.z = blockIdx.x;
  c.x = threadIdx.x;

  // scan 1
  if (local_map.edt_const_at(c.x,c.y,c.z).o)
  {
    local_map.g(c.x,0,c.z,0) = 0;
  }
  else
  {
    local_map.g(c.x,0,c.z,0) = local_map.m_max_width;
  }
  for (c.y=1; c.y<local_map.m_map_size.y; c.y++)
  {
    if (local_map.edt_const_at(c.x,c.y,c.z).o)
    {
      local_map.g(c.x,c.y,c.z,0) = 0;
    }
    else
    {
      local_map.g(c.x,c.y,c.z,0) = 1 + local_map.g(c.x,c.y-1,c.z,0);
    }
  }
  // scan 2
  for (c.y=local_map.m_map_size.y-2;c.y>=0;c.y--)
  {
    if (local_map.g(c.x,c.y+1,c.z,0) < local_map.g(c.x,c.y,c.z,0))
    {
      local_map.g(c.x,c.y,c.z,0) = 1+local_map.g(c.x,c.y+1,c.z,0);
    }
  }
}

//phase 2: each kernel scan along the true x direction
__global__
void phase2(EDTMap local_map)
{
  int z = blockIdx.x;
  int x = threadIdx.x;

  int q=0;
  local_map.s(x,0,z,1) = 0;
  local_map.t(x,0,z,1) = 0;
  for (int u=1;u<local_map.m_map_size.x;u++)
  {
    while (q>=0 && local_map.f(local_map.t(x,q,z,1),local_map.s(x,q,z,1),x,z)
           > local_map.f(local_map.t(x,q,z,1),u,x,z))
    {
      q--;
    }
    if (q<0)
    {
      q = 0;
      local_map.s(x,0,z,1)=u;
    }
    else
    {
      int w = 1+local_map.sep(local_map.s(x,q,z,1),u,x,z);
      if (w < local_map.m_map_size.x)
      {
        q++;
        local_map.s(x,q,z,1)=u;
        local_map.t(x,q,z,1)=w;
      }
    }
  }
  for (int u=local_map.m_map_size.x-1;u>=0;u--)
  {
    local_map.g(x,u,z,1)=local_map.f(u,local_map.s(x,q,z,1),x,z);
    if (u == local_map.t(x,q,z,1))
      q--;
  }
}

//phase 3: each kernel scan along the true z direction
__global__
void phase3(EDTMap local_map)
{
  int z = blockIdx.x;
  int x = threadIdx.x;

  int q = 0;
  local_map.s(x,0,z,2) = 0;
  local_map.t(x,0,z,2) = 0;
  for (int u=1; u<local_map.m_map_size.z; u++)
  {
    while (q>=0 && local_map.fz(local_map.t(x,q,z,2),local_map.s(x,q,z,2),z,x)
           > local_map.fz(local_map.t(x,q,z,2),u,z,x))
    {
      q--;
    }
    if (q<0)
    {
      q = 0;
      local_map.s(x,0,z,2) = u;
    }
    else
    {
      int w = 1+local_map.sepz(local_map.s(x,q,z,2),u,z,x);
      if (w<local_map.m_map_size.z)
      {
        q++;
        local_map.s(x,q,z,2) = u;
        local_map.t(x,q,z,2) = w;
      }
    }
  }
  for (int u=local_map.m_map_size.z-1;u>=0;u--)
  {
    local_map.g(x,u,z,2) = local_map.fz(u,local_map.s(x,q,z,2),z,x);
    if (u==local_map.t(x,q,z,2))
      q--;
  }
}

//phase 4: each kernel scan along the true y direction
__global__
void phase4(CUDA_GEO::coord shift, GridMap glb_map, EDTMap local_map, int map_ct)
{
  CUDA_GEO::coord c;
  c.z = blockIdx.x;
  c.x = threadIdx.x;

  int dx,dy,dz;
  unsigned char val = 0;
  bool seen = 0;
  for (c.y=0;c.y<local_map.m_map_size.y;c.y++)
  {
    //        if (glb_map.isCoordInsideGlobalMap(c+shift))
    //        {
    local_map.edt_at(c.x,c.y,c.z).d=sqrt((float)local_map.h(c.x,c.y,c.z,0));
    //        }
    //        else
    //        {
    //            local_map.buf(c.x,c.y,c.z).d=0;
    //        }

    // Update the seen map
    dx = c.x - local_map.m_map_size.x/2;
    dy = c.y - local_map.m_map_size.y/2;
    dz = c.z - local_map.m_map_size.z/2;
    glb_map.get_pbty(&val,&seen,c+shift,map_ct);
    if (dx*dx+dy*dy+dz*dz<=4 || seen)
      local_map.edt_at(c.x,c.y,c.z).s = true;
    else
      local_map.edt_at(c.x,c.y,c.z).s = false;
  }
}

//phase 4: each kernel scan along the true y direction
// A special mode that only utilize the registered .o flag
// from the local map itself to determin whether a grid is "occupied"
// use together with "phase1_occupancy_mode"
__global__
void phase4_occupancy_mode(EDTMap local_map)
{
  CUDA_GEO::coord c;
  c.z = blockIdx.x;
  c.x = threadIdx.x;

  int dx,dy,dz;
  unsigned char val = 0;
  bool seen = 0;
  for (c.y=0;c.y<local_map.m_map_size.y;c.y++)
  {
    local_map.edt_at(c.x,c.y,c.z).d=sqrt((float)local_map.h(c.x,c.y,c.z,0));
  }
}

__global__
void compress_map(GridMap glb_map, GridMap compressed_map, CUDA_GEO::coord shift, int compress_range, int map_ct)
{
  // Get the local coordinate (start from 0,0,0)
  CUDA_GEO::coord local_c, global_c;
  local_c.x = blockIdx.x;
  local_c.y = threadIdx.x;

  unsigned char val = 0;
  bool seen = 0;
  bool occupied = 0;

  // Scan through each x, y horizontal position with the z belongs to -+compress_range
  for (local_c.z = -compress_range; local_c.z <= compress_range; local_c.z++)
  {
    // Get the global coordinate
    global_c = local_c + shift;

    // Retrieve the occupancy probability
    glb_map.get_pbty(&val,&seen,global_c,map_ct);

    // Determine this x,y grid is occupied
    if (val > 160)
    {
      occupied = true;
      break;
    }
  }

  // When accessing the compressed 2D map, the height of the global coordinate is
  // always determined by the fly height (which is translated into the value shift here)
  // Therefore, set the local coordinate's z value to 0.
  local_c.z = 0;
  global_c = local_c + shift;
  if (occupied)
    compressed_map.update_occupancy(global_c, 250, 1.0f, map_ct);
  else
    compressed_map.update_occupancy(global_c, 0, 1.0f, map_ct);
}

void edt_from_occupancy(EDTMap *local_map, cuttHandle *plan)
{
  // Through rotating the tensor, make sure each kernel is always scanning along its local(after rotation) y direction
  // for fast global memory accessing.
  phase1_occupancy_mode<<<local_map->m_map_size.z,local_map->m_map_size.x>>>(*local_map);
  cuttCheck(cuttExecute(plan[0], local_map->_g, local_map->_h));
  phase2<<<local_map->m_map_size.z,local_map->m_map_size.y>>>(*local_map);
  cuttCheck(cuttExecute(plan[1], local_map->_g, local_map->_h));
  phase3<<<local_map->m_map_size.x,local_map->m_map_size.y>>>(*local_map);
  cuttCheck(cuttExecute(plan[2], local_map->_g, local_map->_h));
  phase4_occupancy_mode<<<local_map->m_map_size.z,local_map->m_map_size.x>>>(*local_map);
}

void edt_3D(GridMap *glb_map, EDTMap *local_map, const CUDA_GEO::coord &shift, cuttHandle *plan, const int map_ct)
{
  // Through rotating the tensor, make sure each kernel is always scanning along its local(after rotation) y direction
  // for fast global memory accessing.
  phase1<<<local_map->m_map_size.z,local_map->m_map_size.x>>>(shift,*glb_map,*local_map,map_ct);
  cuttCheck(cuttExecute(plan[0], local_map->_g, local_map->_h));
  phase2<<<local_map->m_map_size.z,local_map->m_map_size.y>>>(*local_map);
  cuttCheck(cuttExecute(plan[1], local_map->_g, local_map->_h));
  phase3<<<local_map->m_map_size.x,local_map->m_map_size.y>>>(*local_map);
  cuttCheck(cuttExecute(plan[2], local_map->_g, local_map->_h));
  phase4<<<local_map->m_map_size.z,local_map->m_map_size.x>>>(shift,*glb_map,*local_map,map_ct);
}

void edt_2D(GridMap *glb_map, EDTMap *local_map, const CUDA_GEO::coord &shift, cuttHandle *plan, const int map_ct)
{
  // Through rotating the tensor, make sure each kernel is always scanning along its local(after rotation) y direction
  // for fast global memory accessing.
  phase1<<<local_map->m_map_size.z,local_map->m_map_size.x>>>(shift,*glb_map,*local_map,map_ct);
  cuttCheck(cuttExecute(plan[0], local_map->_g, local_map->_h));
  phase2<<<local_map->m_map_size.z,local_map->m_map_size.y>>>(*local_map);
  cuttCheck(cuttExecute(plan[1], local_map->_g, local_map->_h));
  phase4<<<local_map->m_map_size.z,local_map->m_map_size.x>>>(shift,*glb_map,*local_map,map_ct);
}

void compress_3D_map_to_2D(GridMap *glb_map, GridMap *compressed_map, const CUDA_GEO::coord &shift, const float &compress_range, const int map_ct)
{
  int compress_range_in_coord_unit = compress_range/glb_map->m_grid_step;
  compress_map<<<compressed_map->m_update_range.x, compressed_map->m_update_range.y>>>(*glb_map, *compressed_map, shift, compress_range_in_coord_unit, map_ct);
}

}
