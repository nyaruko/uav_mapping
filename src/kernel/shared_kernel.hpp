#ifndef SHARED_KERNEL_HPP
#define SHARED_KERNEL_HPP
#include "edt/gpu_sensors.cuh"
#define USE_DIRECT_METHOD
namespace GPU_SENS
{
//---
__device__ __forceinline__
int coord2idx_shared(const CUDA_GEO::coord& loc_id, int maxX, int maxY)
{
  return loc_id.z*maxX*maxY + loc_id.y*maxX + loc_id.x;
}
//---
__global__ __forceinline__
void update_occupancy_from_status(GridMap glb_map, CUDA_GEO::coord shift, int filter_half_width, int map_ctt)
{
  extern __shared__ unsigned char shared_mem[];
  const int maxX_shared = blockDim.x + 2*filter_half_width;
  const int maxY_shared = blockDim.y + 2*filter_half_width;
  const CUDA_GEO::coord unshifted_id(blockIdx.x*blockDim.x+threadIdx.x, blockIdx.y*blockDim.y+threadIdx.y, blockIdx.z*blockDim.z+threadIdx.z);
  const CUDA_GEO::coord glb_id = unshifted_id + shift;

  const CUDA_GEO::coord loc_id(threadIdx.x, threadIdx.y, threadIdx.z);

  const CUDA_GEO::coord glb_load_id_origin(glb_id.x-filter_half_width, glb_id.y-filter_half_width, glb_id.z-filter_half_width);
  const CUDA_GEO::coord loc_load_id_origin = loc_id;

  const CUDA_GEO::coord glb_load_id_boundary(glb_id.x+filter_half_width, glb_id.y+filter_half_width, glb_id.z+filter_half_width);
  const CUDA_GEO::coord loc_load_id_boundary(loc_id.x+2*filter_half_width, loc_id.y+2*filter_half_width, loc_id.z+2*filter_half_width);

  CUDA_GEO::coord glb_load_id = glb_load_id_origin;
  CUDA_GEO::coord loc_load_id = loc_load_id_origin;

  unsigned char val;
  bool seen;
  shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);

  bool x_boundary=false;
  bool y_boundary=false;
  bool z_boundary=false;

  if (threadIdx.x >= blockDim.x - 2*filter_half_width)
    x_boundary = true;

  if (threadIdx.y >= blockDim.y - 2*filter_half_width)
    y_boundary = true;

  if (threadIdx.z >= blockDim.z - 2*filter_half_width)
    z_boundary = true;

  if (x_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_boundary.x, glb_load_id_origin.y, glb_load_id_origin.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_boundary.x, loc_load_id_origin.y, loc_load_id_origin.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);
  }

  if (y_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_origin.x, glb_load_id_boundary.y, glb_load_id_origin.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_origin.x, loc_load_id_boundary.y, loc_load_id_origin.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);;
  }

  if (z_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_origin.x, glb_load_id_origin.y, glb_load_id_boundary.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_origin.x, loc_load_id_origin.y, loc_load_id_boundary.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);;
  }

  if (x_boundary && y_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_boundary.x, glb_load_id_boundary.y, glb_load_id_origin.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_boundary.x, loc_load_id_boundary.y, loc_load_id_origin.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);;
  }

  if (x_boundary && z_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_boundary.x, glb_load_id_origin.y, glb_load_id_boundary.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_boundary.x, loc_load_id_origin.y, loc_load_id_boundary.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);;
  }


  if (y_boundary && z_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_origin.x, glb_load_id_boundary.y, glb_load_id_boundary.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_origin.x, loc_load_id_boundary.y, loc_load_id_boundary.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);;
  }


  if (x_boundary && y_boundary && z_boundary)
  {
    glb_load_id = CUDA_GEO::coord(glb_load_id_boundary.x, glb_load_id_boundary.y, glb_load_id_boundary.z);
    loc_load_id = CUDA_GEO::coord(loc_load_id_boundary.x, loc_load_id_boundary.y, loc_load_id_boundary.z);
    shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)] = glb_map.get_status(glb_load_id);;
  }

  __syncthreads();

  if(unshifted_id.z >= glb_map.m_update_range.z || unshifted_id.y >= glb_map.m_update_range.y || unshifted_id.x >= glb_map.m_update_range.x)
    return;

  loc_load_id.x = loc_id.x + filter_half_width;
  loc_load_id.y = loc_id.y + filter_half_width;
  loc_load_id.z = loc_id.z + filter_half_width;

  val = shared_mem[coord2idx_shared(loc_load_id,maxX_shared,maxY_shared)];
  if (val == GridMap::OCCUPIED)
  {
    // Obstacle
    glb_map.update_occupancy(glb_id,250,0.8f,map_ctt);
    return;
  }

  if (val == GridMap::FREE)
  {
    // Access all its neighbours
    CUDA_GEO::coord delta,nh;
    for (delta.x = -filter_half_width; delta.x <=filter_half_width; delta.x++)
    {
      for (delta.y = -filter_half_width; delta.y <=filter_half_width; delta.y++)
      {
        for (delta.z = -filter_half_width; delta.z <=filter_half_width; delta.z++)
        {
          if (delta.x == 0 && delta.y == 0 && delta.z == 0)
            continue;

          nh = loc_load_id + delta;
          // find whether the neighbour is in the blocked space
          val = shared_mem[coord2idx_shared(nh,maxX_shared,maxY_shared)];
          if(val == GridMap::BLOCKED)
          {
            return;
          }
        }
      }
    }
    // only when non-of its neigh bour is in the blocked space, we clear this grid
    glb_map.update_occupancy(glb_id,0,0.3f,map_ctt);
  }
}
}
#endif // SHARED_KERNEL_HPP
