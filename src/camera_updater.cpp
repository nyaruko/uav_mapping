#include "edt/camera_updater.h"

CameraUpdater::CameraUpdater(const CameraParams &param):
  m_param(param)
{
  //allocate memory
  m_depth_data_sz=static_cast<size_t>(m_param.rows*m_param.cols)*sizeof(CAM_DATA_TYPE);
  CUDA_ALLOC_DEV_MEM(&m_depth_data,m_depth_data_sz);
}


CameraUpdater::~CameraUpdater()
{
  CUDA_FREE_DEV_MEM(m_depth_data);
}


void CameraUpdater::update_occumap(GridMap *glb_map, const tf::Transform &trans, const sensor_msgs::Image::ConstPtr &depth_img, const int map_ctt)
{
  // construct the projection matrix
  tf::Quaternion Rotq=trans.getRotation();
  tf::Vector3 transVec=trans.getOrigin();

  ProjParams proj;
  proj.L2G = cudaMat::SE3<float>(static_cast<float>(Rotq.w()),static_cast<float>(Rotq.x()),
                                 static_cast<float>(Rotq.y()),static_cast<float>(Rotq.z()),
                                 static_cast<float>(transVec.m_floats[0]),
                                 static_cast<float>(transVec.m_floats[1]),
                                 static_cast<float>(transVec.m_floats[2]));
  proj.G2L = proj.L2G.inv();
  proj.center.x = static_cast<float>(trans.getOrigin().x());
  proj.center.y = static_cast<float>(trans.getOrigin().y());
  proj.center.z = static_cast<float>(trans.getOrigin().z());

  // copy detph data to device
  CAM_DATA_TYPE* depths = (CAM_DATA_TYPE*)&depth_img->data[0];
  CUDA_MEMCPY_H2D(m_depth_data,depths,m_depth_data_sz);

  GPU_SENS::update_occupancy_with_camera(glb_map,m_depth_data,proj,m_param,map_ctt);
}
