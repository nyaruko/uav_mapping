#ifndef CUDA_EDTMAP_CUH
#define CUDA_EDTMAP_CUH
#include <cuda_geometry/cuda_geometry.cuh>

struct SeenDist
{
  float d; // distance
  bool s;  // seen
  bool o; // used for thining algorithm
};

class EDTMap
{
public:
  EDTMap()
  {

  }

  EDTMap(CUDA_GEO::pos m_origin_, float m_grid_step_, int3 m_map_size_):
    m_create_host_cpy(false),
    m_origin(m_origin_),
    m_grid_step(m_grid_step_),
    m_map_size(m_map_size_)
  {
    m_byte_size = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(SeenDist));
    m_max_width = m_map_size.x+m_map_size.y+m_map_size.z;
  }
  //---
  ~EDTMap()
  {

  }
  //---
  void setup_device()
  {
    CUDA_ALLOC_DEV_MEM(&m_sd_map,static_cast<size_t>(m_byte_size));
    CUDA_DEV_MEMSET(m_sd_map,0,static_cast<size_t>(m_byte_size));

    CUDA_ALLOC_DEV_MEM(&_g,static_cast<size_t>(static_cast<int>(sizeof(int))*m_map_size.x*m_map_size.y*m_map_size.z));
    CUDA_ALLOC_DEV_MEM(&_h,static_cast<size_t>(static_cast<int>(sizeof(int))*m_map_size.x*m_map_size.y*m_map_size.z));
    CUDA_ALLOC_DEV_MEM(&_s,static_cast<size_t>(static_cast<int>(sizeof(int))*m_map_size.x*m_map_size.y*m_map_size.z));
    CUDA_ALLOC_DEV_MEM(&_t,static_cast<size_t>(static_cast<int>(sizeof(int))*m_map_size.x*m_map_size.y*m_map_size.z));

    if(m_create_host_cpy)
    {
      m_hst_sd_map = new SeenDist[m_map_size.x*m_map_size.y*m_map_size.z];
    }
  }
  //---
  void free_device()
  {
    CUDA_FREE_DEV_MEM(m_sd_map);
    CUDA_FREE_DEV_MEM(_g);
    CUDA_FREE_DEV_MEM(_h);
    CUDA_FREE_DEV_MEM(_s);
    CUDA_FREE_DEV_MEM(_t);
    if(m_create_host_cpy)
    {
      delete [] m_hst_sd_map;
    }
  }
  //---
  __host__ __device__ __forceinline__
  int to_id(int x, int y, int z, int phase=0) const
  {
    switch (phase)
    {
    case 0:
      return z*m_map_size.x*m_map_size.y+y*m_map_size.x+x;
    case 1:
      return z*m_map_size.y*m_map_size.x+y*m_map_size.y+x;
    case 2:
      return z*m_map_size.y*m_map_size.z+y*m_map_size.y+x;
    default:
      return z*m_map_size.x*m_map_size.y+y*m_map_size.x+x;
    }
  }
  //---
  __device__
  SeenDist& edt_at(int x, int y, int z)
  {
    return m_sd_map[to_id(x,y,z)];
  }
  //---
  __device__
  const SeenDist& edt_const_at(int x, int y, int z) const
  {
    return m_sd_map[to_id(x,y,z)];
  }
  //---
  __device__
  const SeenDist& edt_const_at(int3 crd) const
  {
    return m_sd_map[to_id(crd.x,crd.y,crd.z)];
  }
  //---
  __device__
  int& g(int x, int y, int z, int phase)
  {
    return _g[to_id(x,y,z,phase)];
  }
  //---
  __device__
  int& h(int x, int y, int z, int phase)
  {
    return _h[to_id(x,y,z,phase)];
  }
  //---
  __device__
  int& s(int x, int y, int z, int phase)
  {
    return _s[to_id(x,y,z,phase)];
  }
  //---
  __device__
  int& t(int x, int y, int z, int phase)
  {
    return _t[to_id(x,y,z,phase)];
  }

  __device__
  int f(int y,int i,int x,int z)
  {
      int a = h(x,i,z,1);
      return (y-i)*(y-i) + a*a;
  }
  __device__
  int sep(int i,int u,int x,int z)
  {
      int a = h(x,u,z,1);
      int b = h(x,i,z,1);
      return (u*u-i*i+ a*a-b*b)/(2*(u-i));
  }

  __device__
  int fz(int y,int i,int z,int x,int elipse_compress = 4)
  {
      return elipse_compress*(y-i)*(y-i) + h(x,i,z,2);
  }

  __device__
  int sepz(int i,int u,int z,int x,int elipse_compress = 4)
  {
      return (elipse_compress*u*u-elipse_compress*i*i+h(x,u,z,2)-h(x,i,z,2))/(2*elipse_compress*(u-i));
  }

  __host__
  void clearOccupancy() const
  {
    memset(m_hst_sd_map,0,static_cast<size_t>(m_byte_size));
  }

  __host__
  void setOccupancy(const float3 &p, bool val) const
  {
    int ix = floorf( (p.x - m_origin.x) / m_grid_step + 0.5f);
    int iy = floorf( (p.y - m_origin.y) / m_grid_step + 0.5f);
    int iz = floorf( (p.z - m_origin.z) / m_grid_step + 0.5f);
    if (ix<0 || ix>=m_map_size.x ||
        iy<0 || iy>=m_map_size.y ||
        iz<0 || iz>=m_map_size.z)
    {
      return;
    }
    else
    {
      m_hst_sd_map[to_id(ix,iy,iz)].o = val;
    }
  }

  __host__ __device__
  int3 pos2coord(const float3 & p) const
  {
    int3 output;
    output.x = floorf( (p.x - m_origin.x) / m_grid_step + 0.5f);
    output.y = floorf( (p.y - m_origin.y) / m_grid_step + 0.5f);
    output.z = floorf( (p.z - m_origin.z) / m_grid_step + 0.5f);
    return output;
  }

  __host__ __device__
  bool isInside(const int3 &s) const
  {
    if (s.x<0 || s.x>=m_map_size.x ||
        s.y<0 || s.y>=m_map_size.y ||
        s.z<0 || s.z>=m_map_size.z)
      return false;

    return true;
  }

  __host__
  CUDA_GEO::coord pos2coord(const CUDA_GEO::pos & p) const
  {
    CUDA_GEO::coord output;
    output.x = floorf( (p.x - m_origin.x) / m_grid_step + 0.5f);
    output.y = floorf( (p.y - m_origin.y) / m_grid_step + 0.5f);
    output.z = floorf( (p.z - m_origin.z) / m_grid_step + 0.5f);
    return output;
  }

  __host__
  CUDA_GEO::pos coord2pos(const CUDA_GEO::coord & c) const
  {
    CUDA_GEO::pos output;
    output.x = c.x * m_grid_step + m_origin.x;
    output.y = c.y * m_grid_step + m_origin.y;
    output.z = c.z * m_grid_step + m_origin.z;

    return output;
  }

  __host__
  bool isInside(const CUDA_GEO::coord &s)
  {
    if (s.x<0 || s.x>=m_map_size.x ||
        s.y<0 || s.y>=m_map_size.y ||
        s.z<0 || s.z>=m_map_size.z)
      return false;

    return true;
  }

public:
  bool m_create_host_cpy;
  SeenDist *m_sd_map;
  SeenDist *m_hst_sd_map;
  int m_byte_size;
  CUDA_GEO::pos m_origin;
  float m_grid_step;
  int3 m_map_size;
  int m_max_width;
  int *_g;
  int *_h;
  int *_s;
  int *_t;
};

#endif // CUDA_EDTMAP_CUH
