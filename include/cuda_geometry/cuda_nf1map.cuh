#ifndef CUDA_NF1MAP_CUH
#define CUDA_NF1MAP_CUH

#include <cuda_geometry/cuda_geometry.cuh>

class NF1Map
{
public:
  NF1Map()
  {

  }
  //---
  NF1Map(CUDA_GEO::pos m_origin_, float m_grid_step_, int3 m_map_size_):
    m_origin(m_origin_),
    m_grid_step(m_grid_step_),
    m_map_size(m_map_size_)
  {
    m_byte_size = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(float));
    m_max_width = m_map_size.x+m_map_size.y+m_map_size.z;
  }
  //---
  ~NF1Map()
  {

  }
  //---
  void setup_device()
  {
    CUDA_ALLOC_DEV_MEM(&m_nf1_map,static_cast<size_t>(m_byte_size));
    CUDA_DEV_MEMSET(m_nf1_map,0,static_cast<size_t>(m_byte_size));
  }
  //---
  __device__ __host__
  CUDA_GEO::coord pos2coord(const float3 & p) const
  {
    CUDA_GEO::coord output;
    output.x = static_cast<int>(floor(static_cast<double>((p.x - m_origin.x) / m_grid_step + 0.5f)));
    output.y = static_cast<int>(floor(static_cast<double>((p.y - m_origin.y) / m_grid_step + 0.5f)));
    output.z = static_cast<int>(floor(static_cast<double>((p.z - m_origin.z) / m_grid_step + 0.5f)));
    return output;
  }
  //---
  __device__ __host__
  CUDA_GEO::coord pos2coord(const CUDA_GEO::pos & p) const
  {
    CUDA_GEO::coord output;
    output.x = static_cast<int>(floor(static_cast<double>((p.x - m_origin.x) / m_grid_step + 0.5f)));
    output.y = static_cast<int>(floor(static_cast<double>((p.y - m_origin.y) / m_grid_step + 0.5f)));
    output.z = static_cast<int>(floor(static_cast<double>((p.z - m_origin.z) / m_grid_step + 0.5f)));
    return output;
  }
  //---
  __device__ __host__
  CUDA_GEO::pos coord2pos(const CUDA_GEO::coord & c) const
  {
    CUDA_GEO::pos output;
    output.x = c.x * m_grid_step + m_origin.x;
    output.y = c.y * m_grid_step + m_origin.y;
    output.z = c.z * m_grid_step + m_origin.z;

    return output;
  }
  //---
  void free_device()
  {
    CUDA_FREE_DEV_MEM(m_nf1_map);
  }
  //---
  __host__ __device__ __forceinline__
  int to_id(int x, int y, int z, int phase=0) const
  {
    switch (phase)
    {
    case 0:
      return z*m_map_size.x*m_map_size.y+y*m_map_size.x+x;
    case 1:
      return z*m_map_size.y*m_map_size.x+y*m_map_size.y+x;
    case 2:
      return z*m_map_size.y*m_map_size.z+y*m_map_size.y+x;
    default:
      return z*m_map_size.x*m_map_size.y+y*m_map_size.x+x;
    }
  }

  //---
  __device__
  float nf1_const_at(int x, int y, int z) const
  {
    if (x<0 || x>=m_map_size.x ||
        y<0 || y>=m_map_size.y ||
        z<0 || z>=m_map_size.z)
    {
      return 100.0f;
    }

    return m_nf1_map[to_id(x,y,z)];
  }

public:
  float *m_nf1_map;
  int m_byte_size;
  CUDA_GEO::pos m_origin;
  float m_grid_step;
  int3 m_map_size;
  int m_max_width;
};

#endif // CUDA_NF1MAP_CUH
