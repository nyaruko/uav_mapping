#ifndef CUDA_GRIDMAP_CUH
#define CUDA_GRIDMAP_CUH
#include <cuda_geometry/cuda_geometry.cuh>
//#include <edt/EDTMapUtilities.h>

class GridMap
{
public:
  enum GRIDSTT
  {
    OUT_OF_FRUSTUM = 0,
    OCCUPIED,
    FREE,
    BLOCKED
  };

public:
  GridMap(CUDA_GEO::pos origin, float grid_step, int3 map_size, int3 update_range):
    m_origin(origin),
    m_grid_step(grid_step),
    m_map_size(map_size),
    m_update_range(update_range)

  {
    // allocate and initialize the pmap and fmap
    m_pbty_map_sz = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(unsigned char));
    m_stt_map_sz = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(unsigned char));
    m_seen_map_sz = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(bool));
    m_coord_map_sz = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(CUDA_GEO::coord));
    m_time_map_sz = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(int));
    m_pcl_reg_sz = m_map_size.x*m_map_size.y*m_map_size.z*static_cast<int>(sizeof(int));
  }
  //---
  ~GridMap()
  {

  }
  //---
  void setup_device()
  {
    CUDA_ALLOC_DEV_MEM(&m_pbty_map,static_cast<size_t>(m_pbty_map_sz));
    CUDA_ALLOC_DEV_MEM(&m_stt_map,static_cast<size_t>(m_stt_map_sz));
    CUDA_ALLOC_DEV_MEM(&m_seen_map,static_cast<size_t>(m_seen_map_sz));
    CUDA_ALLOC_DEV_MEM(&m_coord_map,static_cast<size_t>(m_coord_map_sz));
    CUDA_ALLOC_DEV_MEM(&m_time_map,static_cast<size_t>(m_time_map_sz));
    CUDA_ALLOC_DEV_MEM(&m_pcl_reg,static_cast<size_t>(m_pcl_reg_sz));
    CUDA_DEV_MEMSET(m_pbty_map,0,static_cast<size_t>(m_pbty_map_sz));
    CUDA_DEV_MEMSET(m_stt_map,0,static_cast<size_t>(m_stt_map_sz));
    CUDA_DEV_MEMSET(m_seen_map,0,static_cast<size_t>(m_seen_map_sz));
    CUDA_DEV_MEMSET(m_coord_map,0,static_cast<size_t>(m_coord_map_sz));
    CUDA_DEV_MEMSET(m_time_map,0,static_cast<size_t>(m_time_map_sz));
    CUDA_DEV_MEMSET(m_pcl_reg,0,static_cast<size_t>(m_pcl_reg_sz));
  }
  //---
  void free_device()
  {
    CUDA_FREE_DEV_MEM(m_pbty_map);
    CUDA_FREE_DEV_MEM(m_seen_map);
    CUDA_FREE_DEV_MEM(m_coord_map);
    CUDA_FREE_DEV_MEM(m_time_map);
    CUDA_FREE_DEV_MEM(m_pcl_reg);
  }
  //---
  __device__ __host__
  CUDA_GEO::coord pos2coord(const CUDA_GEO::pos & p) const
  {
    CUDA_GEO::coord output;
    output.x = static_cast<int>(floor(static_cast<double>((p.x - m_origin.x) / m_grid_step + 0.5f)));
    output.y = static_cast<int>(floor(static_cast<double>((p.y - m_origin.y) / m_grid_step + 0.5f)));
    output.z = static_cast<int>(floor(static_cast<double>((p.z - m_origin.z) / m_grid_step + 0.5f)));
    return output;
  }
  //---
  __device__ __host__
  CUDA_GEO::pos coord2pos(const CUDA_GEO::coord & c) const
  {
    CUDA_GEO::pos output;
    output.x = c.x * m_grid_step + m_origin.x;
    output.y = c.y * m_grid_step + m_origin.y;
    output.z = c.z * m_grid_step + m_origin.z;

    return output;
  }
  //---
  __device__
  void get_pbty(unsigned char *val, bool* seen, const CUDA_GEO::coord & s, int map_ct) const
  {
    // First calculate the modulo idx
    int idx = coord2idx(s);

    // if the memory location is reported as seen
    // and its uderlying global coord matches s
    if (m_seen_map[idx] && m_coord_map[idx] == s && (map_ct - m_time_map[idx] < 600))
    {
      *val = m_pbty_map[idx];
      *seen = true;
    }
    else
    {
      // treat as unseen and empty
      *val = 0;
      *seen = false;
    }
  }
  //---
  __device__
  bool inside_map(const CUDA_GEO::coord & s) const
  {
    if (s.x<0 || s.x>=m_map_size.x ||
        s.y<0 || s.y>=m_map_size.y ||
        s.z<0 || s.z>=m_map_size.z)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  //---
  __device__
  int positive_modulo(int i, int n) const
  {
    return (i % n + n) % n;
  }
  //---
  __device__
  int coord2idx(const CUDA_GEO::coord &c) const
  {
    return positive_modulo(c.x, m_map_size.x)
        + positive_modulo(c.y, m_map_size.y)*m_map_size.x
        + positive_modulo(c.z, m_map_size.z)*m_map_size.x*m_map_size.y;
  }
  //---
  __device__
  void update_occupancy(const CUDA_GEO::coord &c, float val, float low_pass_param,int map_ctt)
  {
      // First calculate the modulo index
      // BE VERY CAREFUL!!! As there is no write protection here
      int idx = coord2idx(c);

      // Calculate the occupancy probability using a low pass filter
      if (m_seen_map[idx] && m_coord_map[idx] == c)
          val = low_pass_param*val + (1.0f-low_pass_param)*static_cast<float>(m_pbty_map[idx]);
      else
          val = low_pass_param*val + (1.0f-low_pass_param)*0.0f;

      if (val > UCHAR_MAX-1) val = UCHAR_MAX-1;
      if (val < 1) val = 1;

      // Assign the value back to the map
      m_coord_map[idx] = c;
      m_time_map[idx] = map_ctt;
      m_seen_map[idx] = true;
      m_pbty_map[idx] = static_cast<unsigned char>(val);
  }

  __device__
  void register_pcl(const CUDA_GEO::coord &c)
  {
    int idx = coord2idx(c);
    m_pcl_reg[idx] = 1;
  }

  __device__
  void unregister_pcl(const CUDA_GEO::coord &c)
  {
    int idx = coord2idx(c);
    m_pcl_reg[idx] = 0;
  }

  __device__
  int registered_times(const CUDA_GEO::coord &c)
  {
    int idx = coord2idx(c);
    return m_pcl_reg[idx];
  }

  __device__
  void set_status(const CUDA_GEO::coord &c, unsigned char val)
  {
    int idx = coord2idx(c);
    m_stt_map[idx] = val;
  }

  __device__
  unsigned char get_status(const CUDA_GEO::coord &c)
  {
    int idx = coord2idx(c);
    return m_stt_map[idx];
  }
public:
  unsigned char *m_pbty_map; // probability map
  unsigned char *m_stt_map; // probability map
  bool *m_seen_map; // seen map (log a cell is seen or not)
  CUDA_GEO::coord *m_coord_map; // coord map (log the true global cooridnate of a cell)
  int *m_time_map; // time map (log the time when this cell is updated)
  int *m_pcl_reg; // to register this grid is being occupied by the point cloud or not
  CUDA_GEO::pos m_origin;
  float m_grid_step;
  int3 m_map_size;
  int3 m_update_range;
  int m_pbty_map_sz;
  int m_stt_map_sz;
  int m_seen_map_sz;
  int m_coord_map_sz;
  int m_time_map_sz;
  int m_pcl_reg_sz;
};
#endif // CUDA_GRIDMAP_CUH
