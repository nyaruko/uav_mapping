#ifndef CUDA_GEOMETRY_CUH
#define CUDA_GEOMETRY_CUH
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <cassert>
#include <cuda_geometry/matrix.cuh>
#include <cuda_geometry/se3.cuh>

#define CUDA_F_PI 3.1415926535f
namespace CUDA_GEO
{
template <typename T>
struct Vec3
{
    T x,y,z;
    //---
    __device__  __host__
    Vec3():x(0),y(0),z(0)
    {

    }
    //---
    __device__  __host__
    Vec3(T a,T b, T c)
    {
        x=a;
        y=b;
        z=c;
    }
    //---
    __device__ __host__
    T square()
    {
        return x*x+y*y+z*z;
    }
    //---
    __device__  __host__
    Vec3 operator+(Vec3 const& rhs) const
    {
        Vec3 tmp;
        tmp.x = x + rhs.x;
        tmp.y = y + rhs.y;
        tmp.z = z + rhs.z;
        return tmp;
    }
    //---
    __device__  __host__
    Vec3 operator-(Vec3 const& rhs) const
    {
        Vec3 tmp;
        tmp.x = x - rhs.x;
        tmp.y = y - rhs.y;
        tmp.z = z - rhs.z;
        return tmp;
    }
    //---
    __device__  __host__
    Vec3 operator*(T const& rhs) const
    {
        Vec3 tmp;
        tmp.x = x * rhs;
        tmp.y = y * rhs;
        tmp.z = z * rhs;
        return tmp;
    }
    //---
    __device__  __host__
    Vec3 operator/(T const& rhs) const
    {
        Vec3 tmp;
        tmp.x = x / rhs;
        tmp.y = y / rhs;
        tmp.z = z / rhs;
        return tmp;
    }
    //---
    __device__  __host__
    Vec3& operator=(Vec3 const& rhs)
    {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
        return *this;
    }
    //---
    __device__  __host__
    bool operator==(Vec3 const& rhs) const
    {
        if(x==rhs.x && y==rhs.y && z==rhs.z)
            return true;
        else
            return false;
    }
    //---
    __device__  __host__
    bool operator<(Vec3 const& rhs) const
    {
        if(x < rhs.x)
        {
            return true;
        }
        else if (x == rhs.x)
        {
            if (y < rhs.y)
            {
                return true;
            }
            else if (y == rhs.y)
            {
                if (z < rhs.z)
                {
                    return true;
                }
            }
        }
        return false;
    }
    //---
    __device__  __host__
    T& at(int i)
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
            return x;
        }
    }
    //---
    __device__  __host__
    const T& const_at(int i)
    {
        switch (i) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            assert(false);
            return x;
        }
    }
    void print(char name[] )
    {
        printf("%s :(%d , %d, %d) \n",name,x,y,z);
    }
};
typedef Vec3<int> coord;
typedef Vec3<float> pos;
}
//===
static void hdlCudaErr(cudaError err, const char* const func, const char* const file, const int line)
{
    if (err != cudaSuccess)
    {
        std::cerr << "CUDA error at: " << file << ":" << line << std::endl;
        std::cerr << cudaGetErrorString(err) << " " << func << std::endl;
        exit(1);
    }
}
#define CUDA_ALLOC_DEV_MEM(devPtr,size) hdlCudaErr(cudaMalloc(devPtr,size), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_MEMCPY_H2D(dst,src,count) hdlCudaErr(cudaMemcpy(dst,src,count,cudaMemcpyHostToDevice), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_MEMCPY_D2H(dst,src,count) hdlCudaErr(cudaMemcpy(dst,src,count,cudaMemcpyDeviceToHost), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_MEMCPY_D2D(dst,src,count) hdlCudaErr(cudaMemcpy(dst,src,count,cudaMemcpyDeviceToDevice), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_FREE_DEV_MEM(devPtr) hdlCudaErr(cudaFree(devPtr), __FUNCTION__, __FILE__, __LINE__)
#define CUDA_DEV_MEMSET(devPtr,value,count) hdlCudaErr(cudaMemset(devPtr,value,count), __FUNCTION__, __FILE__, __LINE__)
#endif // CUDA_GEOMETRY_CUH
