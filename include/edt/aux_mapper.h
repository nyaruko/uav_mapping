#ifndef AUX_MAPPER_H
#define AUX_MAPPER_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseStamped.h>
#include <gazebo_msgs/GetModelState.h>
#include <geometry_msgs/TransformStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/CameraInfo.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <sensor_msgs/Imu.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <chrono>
#include <cuda_geometry/cuda_geometry.cuh>
#include <cuda_geometry/cuda_edtmap.cuh>
#include <cuda_geometry/cuda_gridmap.cuh>
#include <cpc_aux_mapping/grid_map.h>
#include "edt/laser_updater.h"
#include "edt/camera_updater.h"
#include "nav_msgs/Odometry.h"

//#define USE_SYNC

#ifdef USE_SYNC
#include "message_filters/subscriber.h"
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#endif

#define SHOWPC

class AuxMapper
{
  typedef pcl::PointCloud<pcl::PointXYZ> PntCld;
public:
  AuxMapper();
  ~AuxMapper();

private:
  void construct_map(const ros::TimerEvent&);

  void get_laser_scan(const sensor_msgs::LaserScan::ConstPtr &msg);

  void get_vehicle_pose(const geometry_msgs::PoseStamped::ConstPtr &msg);

  void get_vehicle_odom(const nav_msgs::Odometry::ConstPtr &msg);

  void get_confidnece_map(const sensor_msgs::Image::ConstPtr& msg);

  void get_depth_img(const sensor_msgs::Image::ConstPtr& msg);

  void get_caminfo(const sensor_msgs::CameraInfo::ConstPtr& msg);

  // Pivot point: the global-map coordinate of the local map's origin.
  CUDA_GEO::coord calculate_pivot(const CUDA_GEO::pos &vehicle_pos);

  void setup_map_msg(cpc_aux_mapping::grid_map &msg, EDTMap *map, bool resize = false);

  CUDA_GEO::pos calculate_edt_2d();

  CUDA_GEO::pos calculate_edt_3d();
#ifdef USE_SYNC
  void get_laser_odom(const sensor_msgs::LaserScan::ConstPtr &lsr, const nav_msgs::Odometry::ConstPtr &odom);
#endif
private:
  ros::NodeHandle m_nh;

  ros::Publisher  m_edt_pub;
  ros::Subscriber m_pose_sub;
  ros::Subscriber m_confi_img_sub;
  ros::Subscriber m_caminfo_sub;
  ros::Timer      m_map_timer;

#ifdef USE_SYNC
  message_filters::Subscriber<sensor_msgs::LaserScan> m_laser_sub;
  message_filters::Subscriber<nav_msgs::Odometry> m_odom_sub;
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::LaserScan, nav_msgs::Odometry> MySyncPolicy;
  message_filters::Synchronizer<MySyncPolicy> *m_sync;
#else
  ros::Subscriber m_odom_sub;
  ros::Subscriber m_laser_sub;
  ros::Subscriber m_depth_img_sub;
#endif

  LaserUpdater   *m_laser_updater = nullptr;
  CameraUpdater  *m_camera_updater = nullptr;

  boost::shared_ptr<sensor_msgs::LaserScan>  m_laser_scan;
  boost::shared_ptr<geometry_msgs::PoseStamped>  m_pose;
  boost::shared_ptr<sensor_msgs::Image> m_depth_img, m_confi_img;

  bool m_2D_map_mode;
  bool m_assume_uniform_vertical_world;
  bool m_got_scan, m_got_pose, m_got_depth, m_got_confi_map, m_got_caminfo;
  int m_map_ctt;
  float m_fly_height, m_compress_range;
  bool m_nan_is_infinity;

  GridMap *m_glb_map;
  GridMap *m_2D_glb_map;
  EDTMap *m_edt_map;

  // Rotation plan
  cuttHandle m_rot_plan[3];

  cpc_aux_mapping::grid_map m_edt_map_msg;

#ifdef SHOWPC
  ros::Publisher  m_pnt_cld_pub;
  PntCld::Ptr m_view_pnt_cld;
  SeenDist* m_view_sd_map;
#endif

private:
  void plot_pnt_cld(CUDA_GEO::pos center)
  {
    CUDA_MEMCPY_D2H(m_view_sd_map,m_edt_map->m_sd_map,static_cast<size_t>(m_edt_map->m_byte_size));

    CUDA_GEO::coord shift = m_glb_map->pos2coord(center);
    shift.x -= m_edt_map->m_map_size.x/2;
    shift.y -= m_edt_map->m_map_size.y/2;
    shift.z -= m_edt_map->m_map_size.z/2;


    CUDA_GEO::coord c;
    CUDA_GEO::pos p;
    int idx;
    for (c.x=0;c.x<m_edt_map->m_map_size.x;c.x++)
    {
      for (c.y=0;c.y<m_edt_map->m_map_size.y;c.y++)
      {
        for (c.z=0;c.z<m_edt_map->m_map_size.z;c.z++)
        {
          idx = m_edt_map->to_id(c.x, c.y, c.z);
          float dist = m_view_sd_map[idx].d;
          if (dist*m_glb_map->m_grid_step <= 0.1)
          {
            p = m_glb_map->coord2pos(c+shift);
            pcl::PointXYZ clrP;
            clrP.x = p.x;
            clrP.y = p.y;
            clrP.z = p.z;
            m_view_pnt_cld->points.push_back (clrP);
          }
        }
      }
    }
    pcl_conversions::toPCL(ros::Time::now(), m_view_pnt_cld->header.stamp);
    m_pnt_cld_pub.publish (m_view_pnt_cld);
    m_view_pnt_cld->clear();
  }
};

#endif // AUX_MAPPER_H
