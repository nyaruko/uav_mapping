#ifndef LASER_UPDATER_H
#define LASER_UPDATER_H

#include <edt/gpu_sensors.cuh>
#include <cuda_geometry/cuda_gridmap.cuh>
#include <sensor_msgs/LaserScan.h>
#include <tf/tf.h>

class LaserUpdater
{
public:
  LaserUpdater(const LaserParams &param);
  ~LaserUpdater();
  void update_occumap(GridMap *glb_map, const tf::Transform &trans, const sensor_msgs::LaserScan::ConstPtr &scan, bool assume_uniform_vertical_world, const int map_ct);

public:
  size_t m_depth_data_sz;
  LASER_DATA_TYPE *m_depth_data ;
  LaserParams m_param;

};

#endif // LASER_UPDATER_H
