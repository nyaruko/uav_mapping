#ifndef CAMERA_UPDATER_H
#define CAMERA_UPDATER_H

#include <edt/gpu_sensors.cuh>
#include <cuda_geometry/cuda_gridmap.cuh>
#include <tf/tf.h>
#include <sensor_msgs/Image.h>

class CameraUpdater
{
public:
  CameraUpdater(const CameraParams &param);
  ~CameraUpdater();
  void update_occumap(GridMap *glb_map, const tf::Transform &trans, const sensor_msgs::Image::ConstPtr &depth_img, const int map_ct);

public:
  size_t m_depth_data_sz;
  CAM_DATA_TYPE *m_depth_data ;
  CameraParams m_param;

};

#endif // CAMERA_UPDATER_H
