#ifndef GPU_SENSORS_H
#define GPU_SENSORS_H
#include <cuda_geometry/cuda_edtmap.cuh>
#include <cuda_geometry/cuda_gridmap.cuh>
#include "cutt/cutt.h"

#define LASER_DATA_TYPE float
#define CAM_DATA_TYPE float
struct ProjParams
{
    cudaMat::SE3<float> L2G;
    cudaMat::SE3<float> G2L;
    CUDA_GEO::pos center;
};

struct LaserParams
{
    float max_r, theta_inc, theta_min;
    int scan_num;
    LaserParams(int scan_num_,
                float max_r_, float theta_inc_,
                float theta_min_):
        scan_num(scan_num_),
        max_r(max_r_),
        theta_inc(theta_inc_),
        theta_min(theta_min_)
    {}
};

struct CameraParams
{
    int rows, cols;
    float cx, cy, fx, fy;
    bool nan_is_infinity;
    CameraParams(int rows_, int cols_,
                 float cx_,float cy_,
                 float fx_, float fy_,
                 bool nan_is_infinity_):
        rows(rows_),cols(cols_),
        cx(cx_),cy(cy_),
        fx(fx_),fy(fy_),
        nan_is_infinity(nan_is_infinity_)
    {}
};

namespace GPU_SENS
{
void update_occupancy_with_laser(GridMap *glb_map, LASER_DATA_TYPE *detph_data, ProjParams proj, LaserParams param, bool assume_uniform_vertical_world, int map_ctt);
void update_occupancy_with_camera(GridMap *glb_map, CAM_DATA_TYPE *detph_data, ProjParams proj, CameraParams param, int map_ctt);
}
#endif // GPU_SENSORS_H
