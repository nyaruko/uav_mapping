#ifndef GPU_EDT_H
#define GPU_EDT_H
#include <cuda_geometry/cuda_edtmap.cuh>
#include <cuda_geometry/cuda_gridmap.cuh>
#include "cutt/cutt.h"

namespace GPU_EDT
{
void compress_3D_map_to_2D(GridMap *glb_map, GridMap *compressed_map, const CUDA_GEO::coord &shift, const float &compress_range, const int map_ct);
void edt_3D(GridMap *glb_map, EDTMap *local_map, const CUDA_GEO::coord &shift, cuttHandle *plan, const int map_ct);
void edt_2D(GridMap *glb_map, EDTMap *local_map, const CUDA_GEO::coord &shift, cuttHandle *plan, const int map_ct);
void edt_from_occupancy(EDTMap *local_map, cuttHandle *plan);
}
#endif // GPU_EDT_H
